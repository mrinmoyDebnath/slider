
import "./App.css";
import mySlider1 from './assets/images/mySlider1.png';
import mySlider2 from './assets/images/mySlider2.png';
import mySlider3 from './assets/images/mySlider3.png';
import mySlider4 from './assets/images/mySlider4.png';
import mySlider5 from './assets/images/mySlider5.png';

import SlidePanel from './Component/SlidePanel';

function App() {
  const images = [mySlider1, mySlider2, mySlider3, mySlider4, mySlider5];
  return (
  <>
    <div className="App">
      <header className="App-header">
        <div className="myslidecontainer">
          <div className="s-contain">
            
            <SlidePanel images={images}/> 
          </div>
        </div>
      </header>
    </div>
  </>
  );
}

export default App;
