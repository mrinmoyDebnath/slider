import { useState } from 'react';


function SlidePanel(props) {
    const images = props.images;
    let allSlides = [
        { text: "Enjoy cooking while you co-cook/bake with foodies having same interest & taste preferences", img: images[0], margin: "0%", alt: "img1" },
        { text: "Food brands & restaurants promoting their products to targeted audience", img: images[1], margin: "100%", alt: "img2" },
        { text: "Advanced food-based filtering options & structured recipes", img: images[2], margin: "200%", alt: "img3" },
        { text: "Learn the art & science of cooking or baking from experts", img: images[3], margin: "300%", alt: "img4" },
        { text: "Setup your personal cloud kitchen to deliver food and services in nearest proximity of your base", img: images[4], margin: "400%", alt: "img5" }
    ]
    const [slides, setSlides] = useState(allSlides);


    let slide = document.getElementsByClassName('s-element');

    function nextSlides() {
        let position = parseInt(slide[0].style.marginLeft, 10) / 100;
        //console.log(position, slide.length);
        if (-position < slide.length-1) {
            for (let i = 0; i < slide.length; i++) {
                let val = parseInt(slide[i].style.marginLeft, 10);
                slide[i].style.marginLeft = val - 100 + '%';
            }
        }
    }

    function previousSlides() {
        let position = parseInt(slide[0].style.marginLeft, 10) / 100;
        //console.log(position, slide.length);
        if (-position > 0) {
            for (let i = 0; i < slide.length; i++) {
                let val = parseInt(slide[i].style.marginLeft, 10);
                slide[i].style.marginLeft = val + 100 + '%';
            }
        }
    }

    return (
        <>
            <div>
                <div className="s-control">
                    <div onClick={() => previousSlides()}>
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgBAMAAAAQtmoLAAAAHlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC3KG9qAAAACnRSTlMAb/9XwwyPqwRfnWCbVAAAAGNJREFUeAHtzbENQGAAhNHTqP/YwBFiEL0RxBRKo2MDFw3J9xZ4AoAfqYoyTRsGdskCuw0Du2SB3YWB5zDoFwICgiDQZtvjocdq33ZRUFC8LiaFxaqsGJSow+AqsuAqVgHA9500CjE7GA683AAAAABJRU5ErkJggg==" />
                Previous
            </div>
                    <div onClick={() => nextSlides()}>
                        Next
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAo0lEQVR4Ae3RAQmAQBREQQMYwHYGEDDKD2UI46jAGUDAW4UZ2AJvBwAAAAAAAAAAgM7WNkLxjzYnBOM7IR/fCfn4TnjdfEfOnMB0bXeCE/KcgBOcgBOcgBOcgBOcgBOcgBOcgBOcgBM+bLy2PTigBsQXH/HFR3zxEV98xBcf8cVHfPF7ER/xxWcRP6/Ezyvx80r8vBI/r8QHAAAAAAAAAAD+5gQjn99YlGIBlQAAAABJRU5ErkJggg==" />
                    </div>
                </div>
            </div>
            <div className="s-slidepanel">
                {
                    slides.map((slide, i) =>
                        <div key={i} className="s-element" style={{ marginLeft: slide.margin, transition: ".5s" }}>
                            <div className="w-50 center textp">
                                {slide.text}
                            </div>

                            <div className="w-50 center img-blockfix">
                                <div className="slideimg">
                                    <img src={slide.img} alt={""} />
                                </div>
                            </div>
                        </div>
                    )
                }

            </div>
        </>

    );
}

export default SlidePanel;
